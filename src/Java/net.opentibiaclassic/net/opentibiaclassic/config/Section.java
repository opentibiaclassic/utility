package net.opentibiaclassic.config;

import java.util.Hashtable;

public class Section extends Hashtable<String,String> {

    public boolean containsKey(final String key) {
        return super.containsKey(key.toLowerCase());
    }

    @Override
    public boolean containsKey(final Object key) {
        return this.containsKey((String)key);
    }

    @Override
    public String putIfAbsent(final String key, final String value) {
        return super.putIfAbsent(key.toLowerCase(), value);
    }

    public String remove(final String key) {
        return super.remove(key.toLowerCase());
    }

    @Override
    public String remove(final Object key) {
        return this.remove((String)key);
    }

    @Override
    public String put(final String key, final String value) {
        return super.put(key.toLowerCase(), value);
    }

    public String get(final String key) {
        return super.get(key.toLowerCase());
    }

    @Override
    public String get(final Object key) {
        return this.get((String)key);
    }

    public String getOrDefault(final String key, final String value) {
        return super.getOrDefault(key.toLowerCase(), value);
    }

    @Override
    public String getOrDefault(final Object key, final String value) {
        return this.getOrDefault((String)key, value);
    }

    public Boolean getBool(final String key, final Boolean defaultValue) {
        return this.containsKey(key) ? Boolean.parseBoolean(this.get(key)) : defaultValue;
    }

    public Boolean getBool(final String key) {
        return this.getBool(key, null);
    }

    public String getString(final String key, final String defaultValue) {
        return this.containsKey(key) ? this.get(key) : defaultValue;
    }

    public String getString(final String key) {
        return this.getString(key, null);
    }

    public Byte getByte(final String key, final Byte defaultValue) {
        return this.containsKey(key) ? Byte.parseByte(this.get(key)) : defaultValue;
    }

    public Byte getByte(final String key) {
        return this.getByte(key, null);
    }

    public Short getShort(final String key, final Short defaultValue) {
        return this.containsKey(key) ? Short.parseShort(this.get(key)) : defaultValue;
    }

    public Short getShort(final String key) {
        return this.getShort(key, null);
    }

    public Integer getInt(final String key, final Integer defaultValue ) {
        return this.containsKey(key) ? Integer.parseInt(this.get(key)) : defaultValue;
    }

    public Integer getInt(final String key) {
        return this.getInt(key, null);
    }

    public Long getLong(final String key, final Long defaultValue ) {
        return this.containsKey(key) ? Long.parseLong(this.get(key)) : defaultValue;
    }

    public Long getLong(final String key) {
        return this.getLong(key, null);
    }

    public Float getFloat(final String key, final Float defaultValue ) {
        return this.containsKey(key) ? Float.parseFloat(this.get(key)) : defaultValue;
    }

    public Float getFloat(final String key) {
        return this.getFloat(key, null);
    }

    public Double getDouble(final String key, final Double defaultValue ) {
        return this.containsKey(key) ? Double.parseDouble(this.get(key)) : defaultValue;
    }

    public Double getDouble(final String key) {
        return this.getDouble(key, null);
    }
}