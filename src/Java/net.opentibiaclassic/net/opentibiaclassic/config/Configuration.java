package net.opentibiaclassic.config;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.lang.StringBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.util.stream.Collectors;

import java.util.function.Consumer;

import net.opentibiaclassic.OpenTibiaClassicLogger;

public class Configuration extends Hashtable<String, Section> {
    private static OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(Configuration.class.getCanonicalName());

    private Pattern ignoredPattern = Pattern.compile("^\\s*(#.*)?");
    private Pattern sectionPattern  = Pattern.compile( "^\\s*\\[([^]]+)\\]\\s*$");
    private Pattern keyValuePattern = Pattern.compile( "^\\s*([^=\\s]+)\\s*= ([^#]+)" );
    private Section defaultSection;

    public Configuration() {
        super();
        this.defaultSection = new Section();
    }

    public Configuration(final InputStream inputStream) throws IOException, InvalidConfigurationSyntaxException {
        this();
        this.load(inputStream);
    }

    public Configuration(final Configuration defaults) {
        this();
        this.putAll(defaults);
        this.defaultSection.putAll(defaults.getDefaultSection());
    }

    public synchronized void load(final InputStream inputStream) throws IOException, InvalidConfigurationSyntaxException {
        try (
            final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        ) {
            String sectionName = null;
            Section section = this.defaultSection;

            int i = 0;
            for(String line : bufferedReader.lines().collect(Collectors.toList())) {
                i++;

                logger.debug("read Line(%s)", line);

                if (ignoredPattern.matcher(line).matches()) {
                    logger.debug("ignoredPattern match");
                    continue;
                }

                Matcher m = sectionPattern.matcher(line);
                if (m.matches()) {
                    logger.debug("sectionPattern match");
                    if (null != sectionName) {
                        final String key = sectionName.toLowerCase();
                        if (!this.containsKey(key)) {
                            logger.debug("adding Section(%s)", key);
                            this.put(key, section);
                        } else {
                            logger.debug("merging Section(%s)", key);
                            this.get(key).putAll(section);
                        }
                    }

                    sectionName = m.group(1);
                    logger.debug("creating Section(%s)", sectionName);
                    section = new Section();
                    continue;
                }

                m = keyValuePattern.matcher(line);
                if (m.matches()) {
                    logger.debug("keyValuePattern match");

                    final String key = m.group(1).toLowerCase();
                    final String value = m.group(2);

                    logger.debug("setting Key(%s)=Value(%s)", key, value);
                    section.put(key,value);
                    continue;
                }

                throw new InvalidConfigurationSyntaxException(i, line);
            }

            if (null != sectionName) {
                final String key = sectionName.toLowerCase();
                if (!this.containsKey(key)) {
                    this.put(key, section);
                } else {
                    this.get(key).putAll(section);
                }
            }
        }
    }

    public synchronized void store(final OutputStream outputStream) throws IOException {
        final StringBuffer buffer = new StringBuffer();

        final Consumer<Section> bufferSection = (s) -> {
            synchronized(s) {
                s.entrySet().stream().forEach(e -> {
                    buffer.append(String.format("%s = %s\n", e.getKey(), e.getValue()));
                });
            }
        };

        bufferSection.accept(this.defaultSection);

        this.entrySet().stream().forEach(e -> {
            if (buffer.length() > 0) {
                buffer.append("\n");
            }
            buffer.append(String.format("[%s]\n", e.getKey()));
            bufferSection.accept(e.getValue());
        });

        outputStream.write(buffer.toString().getBytes(StandardCharsets.UTF_8));
    }

    public Section getDefaultSection() {
        return this.defaultSection;
    }


    public boolean containsKey(final String key) {
        return super.containsKey(key.toLowerCase());
    }

    @Override
    public boolean containsKey(final Object key) {
        return this.containsKey((String)key);
    }

    public Section remove(final String key) {
        return super.remove(key.toLowerCase());
    }

    @Override
    public Section remove(final Object key) {
        return this.remove((String)key);
    }

    public Section get(final String key) {
        return super.get(key.toLowerCase());
    }

    @Override
    public Section get(final Object key) {
        return this.get((String)key);
    }

    public Boolean getBool(final String key, final Boolean defaultValue) {
        return this.defaultSection.getBool(key, defaultValue);
    }

    public Boolean getBool(final String key) {
        return this.defaultSection.getBool(key);
    }

    public String getString(final String key, final String defaultValue) {
        return this.defaultSection.getString(key, defaultValue);
    }

    public String getString(final String key) {
        return this.defaultSection.getString(key);
    }

    public Byte getByte(final String key, final Byte defaultValue) {
        return this.defaultSection.getByte(key, defaultValue);
    }

    public Byte getByte(final String key) {
        return this.defaultSection.getByte(key);
    }

    public Short getShort(final String key, final Short defaultValue) {
        return this.defaultSection.getShort(key, defaultValue);
    }

    public Short getShort(final String key) {
        return this.defaultSection.getShort(key);
    }

    public Integer getInt(final String key, final Integer defaultValue ) {
        return this.defaultSection.getInt(key, defaultValue);
    }

    public Integer getInt(final String key) {
        return this.defaultSection.getInt(key);
    }

    public Long getLong(final String key, final Long defaultValue ) {
        return this.defaultSection.getLong(key, defaultValue);
    }

    public Long getLong(final String key) {
        return this.defaultSection.getLong(key);
    }

    public Float getFloat(final String key, final Float defaultValue ) {
        return this.defaultSection.getFloat(key, defaultValue);
    }

    public Float getFloat(final String key) {
        return this.defaultSection.getFloat(key);
    }

    public Double getDouble(final String key, final Double defaultValue ) {
        return this.defaultSection.getDouble(key, defaultValue);
    }

    public Double getDouble(final String key) {
        return this.defaultSection.getDouble(key);
    }
}