package net.opentibiaclassic.config;

public class InvalidConfigurationSyntaxException extends Exception {

    private static String buildMessage(final int lineNumber, final String line) {
        String sample = line;
        if (20 < line.length()) {
            sample = String.format("%s...", line.substring(0, 20));
        }

        return String.format("Invalid INI syntax near '%s' (line, %d)", sample, lineNumber);
    }

    public InvalidConfigurationSyntaxException(final int lineNumber, final String line) {
        super(buildMessage(lineNumber, line));
    }
}