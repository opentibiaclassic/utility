package net.opentibiaclassic;

import java.io.StringWriter;
import java.io.PrintWriter;

import java.util.logging.Logger;
import java.util.logging.Level;

public class OpenTibiaClassicLogger {

    private final Logger parent;

    public OpenTibiaClassicLogger(final String name) {
        parent = Logger.getLogger(name);
    }

    private StackWalker.StackFrame getCallerFrame(final int skip) {
        return StackWalker.getInstance().walk(stream -> stream.skip(skip).findFirst().get());
    }

    // TODO add support for logging strings that arent meant to be format strings (can contain unescaped %)

    private void log(final Level level, final String format, final Object ... args) {
        final StackWalker.StackFrame frame = getCallerFrame(3);

        final String msg = args.length > 0 ? String.format(format, args) : format;

        parent.logp(level, frame.getClassName(), frame.getMethodName(), msg);
    }

    public Level getEffectiveLevel() {
        Logger logger = parent;
        Level level = parent.getLevel();

        while(null == level) {
            logger = logger.getParent();

            if (null != logger)  {
                level = logger.getLevel();
            } else {
                break;
            }
        }

        return level;
    }

    public boolean isDebug() {
        return parent.isLoggable(Level.FINEST);
    }

    public void throwing(final Throwable throwable) {
        final StackWalker.StackFrame frame = getCallerFrame(2);
        parent.throwing(frame.getClassName(), frame.getMethodName(), throwable);
    }

    public void entering(final Object ... parameters) {
        final StackWalker.StackFrame frame = getCallerFrame(2);
        parent.entering(frame.getClassName(), frame.getMethodName(), parameters);
    }

    public void exiting(final Object result) {
        final StackWalker.StackFrame frame = getCallerFrame(2);
        parent.exiting(frame.getClassName(), frame.getMethodName(), result);
    }

    public void debug(final String format, final Object ... args) {
        log(Level.FINEST, format, args);
    }

    public void debug(final Throwable ex) {
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw);

        ex.printStackTrace(pw);

        this.debug(sw.toString().replaceAll("(?:[^%]|\\A)%(?:[^%]|\\z)", "%%"));
    }

    public void info(final String format, final Object ... args) {
        log(Level.INFO, format, args);
    }

    public void warning(final String format, final Object ... args) {
        log(Level.WARNING, format, args);
    }

    public void warning(final Throwable ex) {
        debug(ex);
        log(Level.WARNING, ex.toString());
    }

    public void error(final String format, final Object ... args) {
        log(Level.SEVERE, format, args);
    }

    public void error(final Throwable ex) {
        debug(ex);
        log(Level.SEVERE, ex.toString());
    }
}