package net.opentibiaclassic;

import java.util.logging.LogManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.net.URL;

import java.util.Arrays;
import java.util.UUID;
import java.util.Random;
import java.time.LocalDateTime;
import java.time.Duration;

import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import net.opentibiaclassic.config.Configuration;
import net.opentibiaclassic.config.Section;
import net.opentibiaclassic.config.InvalidConfigurationSyntaxException;

public class Util {
    private static final char DECORATOR = '-';
    private static final int HEADER_DECORATION_LENGTH = 20;
    private static final int HEADER_WIDTH = 80;

    public static String repeat(final char c, final int length) {
        final char[] s = new char[length];
        Arrays.fill(s, c);
        return new String(s);
    }

    private static String formatHeader(final String header) {

        if (header.length() > HEADER_WIDTH) {
            return header;
        }

        final int padFront = (HEADER_WIDTH - header.length())/2;
        final int padEnd = HEADER_WIDTH - header.length() - padFront;

        return String.format("%s %s %s", repeat(DECORATOR, padFront) , header, repeat(DECORATOR, padEnd));
    }

    public static void printHeader(final String header) {
        System.out.println(formatHeader(header));
    }

    public static void printFooter(final String header) {
        System.out.println(repeat(DECORATOR, HEADER_WIDTH));
    }

    public static String formatBytes(final byte[] arr) {
        final String[] formatted = new String[arr.length];

        for (int i=0; i<arr.length; i++) {
            formatted[i] = String.format("0x%02X", arr[i]);
        }

        return String.join(" ", formatted);
    }

    public static void printBytes(final byte[] arr) {
        System.out.printf("%s", formatBytes(arr));
    }

    /**
     * UUID spec for version 1 uses MAC address, replace with random here 
     * */
    private static long generateLeastSignificantBits() {
        Random random = new Random();
        long random63BitLong = random.nextLong() & 0x3FFFFFFFFFFFFFFFL;
        long variant3BitFlag = 0x8000000000000000L;
        return random63BitLong + variant3BitFlag;
    }

    private static long generateMostSignificantBits() {
        LocalDateTime start = LocalDateTime.of(1582, 10, 15, 0, 0, 0);
        Duration duration = Duration.between(start, LocalDateTime.now());
        long seconds = duration.getSeconds();
        long nanos = duration.getNano();
        long timeForUuidIn100Nanos = seconds * 10000000 + nanos * 100;
        long least12SignificatBitOfTime = (timeForUuidIn100Nanos & 0x000000000000FFFFL) >> 4;
        long version = 1 << 12;
        return (timeForUuidIn100Nanos & 0xFFFFFFFFFFFF0000L) + version + least12SignificatBitOfTime;
    }

    public static UUID generateRandomTimeBasedUUID() {
        return new UUID(generateMostSignificantBits(), generateLeastSignificantBits());
    }

    private static final Pattern wordWithSpacePattern = Pattern.compile("[^\\s]*\\s*");
    public static String lineWrap(final String input, final int maxLineLengthChars, final int maxLineEndSpace) {
        final int maxBreakLineLengthChars = maxLineLengthChars - 1;
        final String WORD_BREAK = "-";
        final String NEW_LINE = "\n";

        final Matcher m = wordWithSpacePattern.matcher(input);
        final StringBuffer output = new StringBuffer();
        final StringBuffer line = new StringBuffer(maxLineLengthChars + 1);

        while (m.find()) {
            String wordWithSpacing = m.group();

            if (line.length() + wordWithSpacing.length() <= maxLineLengthChars) {
                line.append(wordWithSpacing);
            } else {
                String trimmed = wordWithSpacing.trim();
                boolean canHandleTrim = line.length() + trimmed.length() <= maxLineLengthChars;

                if (canHandleTrim) {
                    line.append(trimmed);
                } else if (maxLineLengthChars - line.length() >= maxLineEndSpace) {
                    int i = maxLineLengthChars - 1 - line.length();
                    line.append(wordWithSpacing.substring(0, i));
                    line.append(WORD_BREAK);
                    wordWithSpacing = wordWithSpacing.substring(i);
                }

                output.append(line.toString());
                output.append(NEW_LINE);

                while(wordWithSpacing.length() > maxBreakLineLengthChars) {
                    output.append(wordWithSpacing.substring(0, maxBreakLineLengthChars));
                    output.append(WORD_BREAK);
                    output.append(NEW_LINE);
                    wordWithSpacing = wordWithSpacing.substring(maxBreakLineLengthChars);
                }

                line.delete(0, line.length());

                if (!canHandleTrim) {
                    line.append(wordWithSpacing);
                }
            }

            if (wordWithSpacing.endsWith("\n")) {
                output.append(line);
                line.delete(0, line.length());
            }
        }

        output.append(line.toString());
        return output.toString();
    }

    public static String lineWrap(final String input, final int maxLineLengthChars) {
        return lineWrap(input, maxLineLengthChars, 8);
    }

    public static String lineWrap(final String input) {
        return lineWrap(input, 80);
    }

    public static String formatterEscape(final String line) {
        final StringBuilder builder = new StringBuilder();
        final StringBuilder buffer = new StringBuilder();

        for (int i = 0; i < line.length(); i++) {
            char c = line.charAt(i);
            if ('%' == c) {
                buffer.append(c);
            } else {
                if (buffer.length() > 0) {
                    final String b = buffer.toString();
                    
                    if (buffer.length() % 2 != 0) {
                        builder.append(b);
                    }
                    
                    builder.append(b);
                    buffer.setLength(0);
                }

                builder.append(c);
            }
            
        }
        
        return builder.toString();
    }

    public static Configuration loadConfiguration(final URL defaultConfigURL) throws IOException, InvalidConfigurationSyntaxException {
        final Configuration defaults = new Configuration();
        try (InputStream in = defaultConfigURL.openStream()) {
            defaults.load(in);
        }

        final Configuration config = new Configuration(defaults);
        try (final InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("config.ini")) {
            if (null != in) {
                config.load(in);
            }
        }

        final Section loggingSection = config.get("LOGGING");
        if (null != loggingSection && loggingSection.containsKey("PROPERTIES_FILE")) {
            try (final InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(loggingSection.get("PROPERTIES_FILE"))) {
                // NOTE resets the LogManager and will break any loggers created before the call
                LogManager.getLogManager().readConfiguration(in);
            }
        }

        return config;
    }
}