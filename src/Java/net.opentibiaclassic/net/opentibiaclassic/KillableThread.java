package net.opentibiaclassic;

import java.util.UUID;
import java.util.Collections;
import java.util.Set;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Arrays;

import net.opentibiaclassic.OpenTibiaClassicLogger;
import static net.opentibiaclassic.Util.generateRandomTimeBasedUUID;

public abstract class KillableThread extends Thread implements AutoCloseable {
    private static OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(KillableThread.class.getCanonicalName());

    private static final class DEFAULTS {
        public static final class CHILD_THREAD {
            public static final int TIMEOUT_MILLISECONDS = 60;
            public static final int WAIT_TIME_MILLISECONDS = 10;
        }
    }

    public final Set<KillableThread> children;
    private final int childThreadWaitTimeMilliseconds;
    private final int childThreadTimeoutMilliseconds;
    private boolean animated, born;
    public final UUID id;

    public static void animateAll(final Set<KillableThread> group) {
        synchronized (group) {
            for (KillableThread k : group) {
                k.animate();
            }
        }
    }

    public static void animateAll(final KillableThread ... group) {
        animateAll(new HashSet<KillableThread>(Arrays.asList(group)));
    }

    public static void killAll(final Set<KillableThread> group) {
        synchronized (group) {
            for (KillableThread k : group) {
                k.kill();
            }
        }
    }

    public static void killAll(final KillableThread ... group) {
        killAll(new HashSet<KillableThread>(Arrays.asList(group)));
    }

    public static boolean areAnyDead(final Set<KillableThread> group) {
        synchronized (group) {
            for (KillableThread k : group) {
                if (k.isDead()) {
                    return true;
                }
            }

            return false;
        }
    }

    public static void suicidePact(final int waitTimeMilliseconds, final Set<KillableThread> group) {
        final long start = System.currentTimeMillis();
        final long end;
        if (0 == waitTimeMilliseconds) {
            end = Long.MAX_VALUE;
        } else {
            end = start + waitTimeMilliseconds;
        }

        while (!areAnyDead(group) && System.currentTimeMillis() <= end) {
            synchronized (group) {
                try {
                    group.iterator().next().join(1);
                } catch(final NoSuchElementException|InterruptedException ex) {
                    return;
                }
            }
        }
    }

    public static void suicidePact(final int waitTimeMilliseconds, final KillableThread ... group) {
        suicidePact(waitTimeMilliseconds, new HashSet<KillableThread>(Arrays.asList(group)));
    }

    public static void suicidePact(final Set<KillableThread> group) {
        suicidePact(0, group);
    }

    public static void suicidePact(final KillableThread ... group) {
        suicidePact(0, group);
    }

    public final void animateChildren() {
        animateAll(this.children);
    }

    public final void killChildren() {
        killAll(this.children);
    }

    private void interruptChildren() {
        synchronized (this.children) {
            for(KillableThread child : this.children) {
                child.interrupt();
            }
        }
    }

    public KillableThread(final int waitTimeMilliseconds, final int timeoutMilliseconds) {
        super();

        this.childThreadWaitTimeMilliseconds = waitTimeMilliseconds;
        this.childThreadTimeoutMilliseconds = timeoutMilliseconds;
        this.born = false;
        this.animated = false;
        this.id = generateRandomTimeBasedUUID();
        this.children = Collections.synchronizedSet(new HashSet<KillableThread>());
    }

    public KillableThread(final int waitTimeMilliseconds) {
        this(waitTimeMilliseconds, DEFAULTS.CHILD_THREAD.TIMEOUT_MILLISECONDS);
    }

    public KillableThread() {
        this(DEFAULTS.CHILD_THREAD.WAIT_TIME_MILLISECONDS);
    }

    public final boolean isTerminated() {
        return this.getState() == Thread.State.TERMINATED;
    }

    public final boolean notIsTerminated() {
        return !this.isTerminated();
    }

    public final boolean hasChildren() {
        return this.children.size() > 0;
    }

    public final synchronized void animate() {
        this.born = true;
        this.animated = true;
        this.animateChildren();
        this.start();
    }

    public final synchronized void kill() {
        this.animated = false;
    }

    public final synchronized boolean wasBorn() {
        return this.born;
    }

    public final synchronized boolean isAnimated() {
        return this.animated == true;
    }

    public final synchronized boolean isDead() {
        return this.animated == false;
    }

    private void reapChildren() {
        synchronized (this.children) {
            this.children.removeIf(t -> t.isTerminated() || t.isInterrupted());
        }
    }

    public abstract void work() throws Exception;
    public final void run() {
        while (this.isAnimated()) {
            try {
                this.work();
                this.reapChildren();
            } catch(final Exception logged) {
                logger.debug(logged);
                logger.warning(logged);

                this.kill();
            }
        }

        this.close();
    }

    protected void onDeath() {}
    public final void close() {
        try {
            this.killChildren();

            final long endTimeMilliseconds = System.currentTimeMillis() + this.childThreadTimeoutMilliseconds;
            boolean isMoreTime = System.currentTimeMillis() < endTimeMilliseconds;

            while(isMoreTime && this.hasChildren()) {
                this.reapChildren();

                Thread.sleep(this.childThreadWaitTimeMilliseconds);

                isMoreTime = System.currentTimeMillis() < endTimeMilliseconds;
            }

            this.interruptChildren();

            this.reapChildren();
        } catch(final Exception ignored) {}
        finally {
            this.onDeath();
        }
    }
}