#!/bin/bash
set -x
rm -rf build/*
find src/Java -name "*.java" > source_files.log
javac -d build/Java --module-source-path src/Java @source_files.log